REFPATH = "ref/"
RAWPATH = "raw/"
RESULTPATH = "results/"

TOPLEVEL_HUMAN = "Homo_sapiens.GRCh38.dna.toplevel.fa.gz"
TOPLEVEL_HUMAN_DOWNLOAD = "ftp://ftp.ensembl.org/pub/release-98/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz"
CDNA_HUMAN = "Homo_sapiens.GRCh38.cdna.all.fa.gz"
CDNA_HUMAN_DOWNLOAD = "ftp://ftp.ensembl.org/pub/release-98/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh38.cdna.all.fa.gz"
TOPLEVEL_MOUSE = "Mus_musculus.GRCm38.dna.toplevel.fa.gz"
TOPLEVEL_MOUSE_DOWNLOAD = "ftp://ftp.ensembl.org/pub/release-98/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.toplevel.fa.gz"
CDNA_MOUSE = "Mus_musculus.GRCm38.cdna.all.fa.gz"
CDNA_MOUSE_DOWNLOAD = "ftp://ftp.ensembl.org/pub/release-98/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz"

MOUSE_EXOME_LINK = "https://sra-pub-src-1.s3.amazonaws.com/SRR9130497/"

MOUSE_EXOMS = ["BALBc-M1-normal_1.fq.gz.1","BALBc-M1-normal_2.fq.gz.1"]

IDS = [i[:-10] for i in MOUSE_EXOMS[::2]]

rule all:
    input: expand(RESULTPATH + "{sample}-k25-t8.classify.log", sample=IDS)
    
rule classify_mouse_exomes:
    input:
        index = REFPATH + "xengsort-index-k{k}.h5",
        in1 = RAWPATH + "{sample}_1.fq.gz.1",
        in2 = RAWPATH + "{sample}_2.fq.gz.1",
    log:
        RESULTPATH + "{sample}-k{k}-t{t}.classify.log"
    benchmark:
        RESULTPATH + "{sample}-k{k}-t{t}.classify.benchmark"
    shell:
        "(time xengsort classify --name {wildcards.sample} --index {input.index} --classification new -T {wildcards.t} --fastq <(zcat {input.in1}) --pairs <(zcat {input.in2})) &> {log}"

rule build_index:
    input:
        human_tl = REFPATH + TOPLEVEL_HUMAN,
        mouse_tl = REFPATH + TOPLEVEL_MOUSE,
        human_cdna = REFPATH + CDNA_HUMAN,
        mouse_cdna = REFPATH + CDNA_MOUSE
    output:
        REFPATH + "xengsort-index-k{k}.h5"
    log:
        REFPATH + "build-k{k}.log"
    benchmark:
        REFPATH + "build_k{k}.benchmark"
    threads: 8
    shell:
        "(time xengsort index {output} -H <(zcat {input.human_tl}) <(zcat {input.human_cdna}) -G <(zcat {input.mouse_tl}) <(zcat {input.mouse_cdna}) -k {wildcards.k} -P 4800000000 3FCVbb:u --hashfunctions linear945:linear9123641:linear349341847 -p 4 --fill 0.94 -T {threads}) &> {log}"

rule download_refs:
    output:
        REFPATH + TOPLEVEL_HUMAN,
        REFPATH + TOPLEVEL_MOUSE,
        REFPATH + CDNA_HUMAN,
        REFPATH + CDNA_MOUSE
    shell:
        """
            wget {TOPLEVEL_HUMAN_DOWNLOAD} -P {REFPATH}
            wget {TOPLEVEL_MOUSE_DOWNLOAD} -P {REFPATH}
            wget {CDNA_HUMAN_DOWNLOAD} -P {REFPATH}
            wget {CDNA_MOUSE_DOWNLOAD} -P {REFPATH}
        """
        
rule download_mouse_exomes:
    output:
        RAWPATH + "{filename}"
    shell:
        """
            wget {MOUSE_EXOME_LINK}{wildcards.filename} -P {RAWPATH}
        """
