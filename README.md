# xengsort: Fast lightweight accurate xenograft sorting

This tool, xengsort, uses 3-way bucketed Cuckoo hashing to efficiently solve the xenograft sorting problem.
For a description of the method an evaluation on several datasets, please see our [preprint on bioRxiv](https://www.biorxiv.org/content/10.1101/2020.05.14.095604v2).


## Tutorial: Classification of human-captured mouse exomes

We here provide an example showing how to run xengsort on human-captures mouse exomes (one of the datasets described in the paper).
Our implementation is provided as a Python package.
For efficiency, it uses just-in-time compilation provided by the numba package. 
We recommend to use conda to manage  Python packages and separate environments for each application.


## Install the conda package manager (miniconda)
Go to https://docs.conda.io/en/latest/miniconda.html and download the Miniconda installer:
Choose Python 3.7 (or higher), your operating system, and preferably the 64 bit version. 
Follow the instructions of the installer and append the conda executable to your PATH (even if the installer does not recommend it). 
You can let the installer do it, or do it manually by editing your ``.bashrc`` or similar file under Linux or MacOS, or by editing the environment varialbes under Windows.
To verify that the installation works, open a new terminal and execute
```
conda --version # ideally 4.5.xx or higher
python --version # ideally 3.7.xx or higher
```

## Obtain our software. 
Our software is currently obtained by cloning this public git repository:
```
git clone https://gitlab.com/genomeinformatics/xengsort.git
```


## Create and activate new conda environment.
To run our software, a conda environment with the required libraries needs to be created.
A list of needed libraries is provided in the ``requirements.txt`` file in the cloned repository;
it can be used to create a new environment:
```
cd xengsort  # the directory of the cloned repository
conda create --name xengsort --file requirements.txt
```
This may take some time, as conda searches and downloads packages.
After all dependencies are resolved, you activate the environment and install the package from the repository into this environment:
```
conda activate xengsort  # activate environment
python setup.py develop  # install xengsort package into environment
```
The latter command has to be executed from the root directory of the cloned repository (where
``setup.py`` as well as ``requirements.txt`` are found).


## Run Snakemake example workflow
We provide an example Snakemake workflow which downloads all needed references and FASTQ files,  generates a hash table as an index and classifies the reads.
This should be executed in some separate directory with lots of space for the datset.

```
snakemake -s snakefile -j 8
```
