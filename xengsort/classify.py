import numpy as np
from numba import njit, jit, uint32, prange
import datetime
import io
import contextlib

from .hashio import load_hash
from .build import generate_kmer_iterator, all_fasta_seqs
from .dnaio import fastq_reads, fastq_chunks, fasta_reads, fastq_chunks_paired
from .dnaencode import generate_revcomp_and_canonical_code, quick_dna_to_2bits, twobits_to_dna_inplace, make_twobit_to_codes, twobits_to_dna_inplace
import os.path
from concurrent.futures import Executor, ThreadPoolExecutor, as_completed
from math import ceil
DEBUG = True

def make_classify_read(shp, rcmode, h):
    
    rc, cc = generate_revcomp_and_canonical_code(shp, rcmode)
    k, kmers = generate_kmer_iterator(shp, rcmode)        
    get_value = h.get_value

    @njit()
    def classify_read(ht, seq):        
        counts = np.zeros(8, np.uint64) # Wastes two uint64 for fast access
        for code in kmers(seq, 0, len(seq)):
            value = max(get_value(ht,code), 0)# An not included element is representet counted at position 0 instead of -1
            counts[value] += 1
        return counts
        
    return classify_read

@njit(nogil=True)
def classify_as_xenome(counts):
    if counts[1] > 0 and counts[2] > 0: # ambigious
        return 2
    elif counts[1] > 0 and counts[2] == 0: # host
        return 0
    elif counts[1] == 0 and counts[2] > 0: # graft
        return 1
    elif counts[1] == 0 and counts[2] == 0 and sum(counts) != 0: # both
        return 3
    else: # neither
        return 4

@njit(nogil=True)
def majority_vote_classify(counts):
    if counts[1] > 0 and counts[2] == 0: # host
        return 0
    elif counts[1] == 0 and counts[2] > 0: # graft
        return 1
    elif counts[1] > 0 and counts[2] > 0:
        if counts[1] + counts[5] > (counts[2] + counts[6])*99: # ambiguous host
            return 0
        elif (counts[2] + counts[6]) > (counts[1] + counts[5])*99: # ambiguous graft
            return 1
        else: # ambiguous
            return 2
    elif counts[1] == 0 and counts[2] == 0 and sum(counts) != 0: # both
        return 3
    else: # neither
        return 4

@njit(nogil=True, locals=dict(gscore=uint32, hscore=uint32))
def xengsort_classify(counts):
    nkmers=0
    for i in counts:
        nkmers += i
    if nkmers == 0:
        return 2
    Mg = nkmers // 4
    Mh = nkmers // 4
    Mb = nkmers // 5
    gscore = counts[2] + counts[6] // 2
    hscore = counts[1] + counts[5] // 2
    
    if counts[2] >= 6 and counts[5] <= 6 and counts[1] == 0: # host
        return 1
    elif counts[1] >= 6 and counts[6] <= 6 and counts[2] == 0: # graft
        return 0
    
    if counts[2] + counts[6] >= Mg and counts[1] <= nkmers // 20 and counts[5] < gscore: # graft
        return 1
    elif counts[1] + counts[5] >= Mh and counts[2] <= nkmers // 20 and counts[6] < hscore: # host
        return 0
    elif counts[3] + counts[7] >= Mb and gscore <= nkmers // 20 and hscore <= nkmers // 20: # both
        return 3
    elif counts[0] > nkmers * 3 // 4: # neither
        return 4
    else:
        return 2

@njit(nogil=True)
def xengsort_classify_two(counts):
    nkmers=0
    for i in counts:
        nkmers += i
    if nkmers == 0:
        return 2
    Mg = nkmers // 4
    Mh = nkmers // 4
    Mb = nkmers // 5

    if counts[1] + counts[5] == 0: # no host
        gscore = counts[2] + counts[6] // 2
        if gscore >= 3:
            return 1
        elif counts[3] + counts[7] >= Mb: # both
            return 3
        elif counts[0] > nkmers * 3 // 4: # neither
            return 4

    elif counts[2] + counts[6] == 0: # no graft
        hscore = counts[1] + counts[5] // 2
        if hscore >= 3:
            return 0
        elif counts[3] + counts[7] >= Mb: # both
            return 3
        elif counts[0] > nkmers * 3 // 4: # neither
            return 4
    return xengsort_classify(counts)

def make_classify_read_from_fastq(mode, shp, rcmode, bits, path, name, h, threads, pairs, bufsize=2**23, chunkreads=2**23//200, quick=False,# sort=False,
 filt=False, count=False):
    if mode == "xenome" or bits == 2:
        classify = classify_as_xenome
    elif mode == "mv":
        classify = majority_vote_classify
    elif mode == "new":
        classify = xengsort_classify_two
    else:
        assert()

    classify_read = make_classify_read(shp, rcmode, h)
    _, twobit_to_code = make_twobit_to_codes(shp, rcmode)
    get_value = h.get_value

    @njit(nogil=True)
    def get_classification(ht, sq):
        quick_dna_to_2bits(sq)
        if quick:
            third = max(get_value(ht,twobit_to_code(sq, 2)), 0)
            third = third & 3
            thirdlast = max(get_value(ht,twobit_to_code(sq, len(sq)-shp-3)), 0)
            thirdlast = thirdlast & 3 
            if third == thirdlast and third != 3 and third != 0:
                return third - 1
        kcount = classify_read(ht, sq)
        return classify(kcount)

    @njit(nogil=True)
    def get_paired_classification(ht, sq1, sq2):
        quick_dna_to_2bits(sq1)
        quick_dna_to_2bits(sq2)
        if quick:
            third_sq1  = max(get_value(ht,twobit_to_code(sq1, 2)), 0)
            thirdlast_sq1 = max(get_value(ht,twobit_to_code(sq1, len(sq2)-shp-3)), 0)
            third_sq2  = max(get_value(ht,twobit_to_code(sq2, 2)), 0)
            thirdlast_sq2 = max(get_value(ht,twobit_to_code(sq2, len(sq2)-shp-3)), 0)
            third_sq1 = third_sq1 & 3
            third_sq2 = third_sq2 & 3
            thirdlast_sq1 = thirdlast_sq1 & 3
            thirdlast_sq2 = thirdlast_sq2 & 3
            if third_sq1 == thirdlast_sq1 and third_sq1 == third_sq2 & 3 and third_sq1 == thirdlast_sq2  and third_sq1 != 3 and third_sq1 != 0:
                return third_sq1 - 1
        kcount = classify_read(ht, sq1)
        kcount += classify_read(ht, sq2)
        return classify(kcount)

    @njit(nogil=True)
    def classify_kmers_chunkwise(buf, linemarks, ht):#, atsv, btsv, ntsv, htsv, gtsv):
        n = linemarks.shape[0]
        classifications = np.zeros(n, dtype=np.uint8)
        for i in range(n):
            sq = buf[linemarks[i,0]:linemarks[i,1]]
            classifications[i] = get_classification(ht, sq)
            twobits_to_dna_inplace(buf, linemarks[i,0], linemarks[i,1])
        return (classifications, linemarks)

    @njit(nogil=True)
    def classify_paired_kmers_chunkwise(buf, linemarks, buf1, linemarks1, ht):#, atsv, btsv, ntsv, htsv, gtsv):
        n = linemarks.shape[0]
        classifications = np.zeros(n, dtype=np.uint8)
        for i in range(n):
            sq1 = buf[linemarks[i,0]:linemarks[i,1]]
            sq2 = buf1[linemarks1[i,0]:linemarks1[i,1]]
            classifications[i] = get_paired_classification(ht, sq1, sq2)
            twobits_to_dna_inplace(buf, linemarks[i,0], linemarks[i,1])
            twobits_to_dna_inplace(buf1, linemarks1[i,0], linemarks1[i,1])
        return (classifications, linemarks, linemarks1)

    @njit(nogil=True)
    def get_border(linemarks, threads):
        borders = np.empty(threads+1, dtype=uint32)
        for i in range(threads):
            borders[i] = min(ceil(linemarks.shape[0]/threads) * i, linemarks.shape[0])
        borders[threads] = linemarks.shape[0]
        return borders

    class dummycontext():
        def __enter__(self):
            return self

        def __exit__(self, *exc):
            return False

        def write(*_):
            pass

        def flush(*_):
            pass

    @contextlib.contextmanager
    def cond_context_mgr(name, suffix, buffsize, count, filt):
        assert count == False or filt == False
        if count:
            yield dummycontext()
        elif filt:
            if "graft" in suffix:
                yield io.BufferedWriter(io.FileIO(name + suffix, 'w'))
            else:
                yield dummycontext()
        else:
            yield io.BufferedWriter(io.FileIO(name + suffix, 'w'))


    def classify_read_from_fastq(fastq, ht):#, atsv, btsv, ntsv, htsv, gtsv):
        counts = np.zeros(5, np.uint64)
        contexts = [contextlib.nullcontext() for i in range(10)]
        if pairs:
            with ThreadPoolExecutor(max_workers=threads) as executor, \
             cond_context_mgr(name, "-host.1.fq", bufsize, count, filt) as host1, \
             cond_context_mgr(name, "-graft.1.fq", bufsize, count, filt) as graft1, \
             cond_context_mgr(name, "-both.1.fq", bufsize, count, filt) as both1, \
             cond_context_mgr(name, "-neither.1.fq", bufsize, count, filt) as neither1, \
             cond_context_mgr(name, "-ambiguous.1.fq", bufsize, count, filt) as ambiguous1, \
             cond_context_mgr(name, "-host.2.fq", bufsize, count, filt) as host2, \
             cond_context_mgr(name, "-graft.2.fq", bufsize, count, filt) as graft2, \
             cond_context_mgr(name, "-both.2.fq", bufsize, count, filt) as both2, \
             cond_context_mgr(name, "-neither.2.fq", bufsize, count, filt) as neither2, \
             cond_context_mgr(name, "-ambiguous.2.fq", bufsize, count, filt) as ambiguous2:

                for chunk in fastq_chunks_paired((fastq, pairs), bufsize=bufsize*threads, maxreads=chunkreads*threads):
                    borders = get_border(chunk[1], threads)
                    futures = [executor.submit(classify_paired_kmers_chunkwise, chunk[0], chunk[1][borders[i]:borders[i+1]], chunk[2], chunk[3][borders[i]:borders[i+1]], ht) for i in range(threads)]
                    for i in as_completed(futures):
                        (classifications, linemarks, linemarks2) = i.result()
                        for seq in range(linemarks.shape[0]):
                            if classifications[seq] == 0:
                                host1.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                host2.write(chunk[2][linemarks2[seq][2]:linemarks2[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 1:
                                graft1.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                graft2.write(chunk[2][linemarks2[seq][2]:linemarks2[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 2:
                                ambiguous1.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                ambiguous2.write(chunk[2][linemarks2[seq][2]:linemarks2[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 3:
                                both1.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                both2.write(chunk[2][linemarks2[seq][2]:linemarks2[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 4:
                                neither1.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                neither2.write(chunk[2][linemarks2[seq][2]:linemarks2[seq][3]])
                                counts[classifications[seq]] += 1

                host1.flush()
                host2.flush()
                graft1.flush()
                graft2.flush()
                ambiguous1.flush()
                ambiguous2.flush()
                both1.flush()
                both2.flush()
                neither1.flush()
                neither2.flush()

        else:
            with ThreadPoolExecutor(max_workers=threads) as executor, \
             cond_context_mgr(name, "-host.fq", bufsize, count, filt) as host, \
             cond_context_mgr(name, "-graft.fq", bufsize, count, filt) as graft, \
             cond_context_mgr(name, "-both.fq", bufsize, count, filt) as both, \
             cond_context_mgr(name, "-neither.fq", bufsize, count, filt) as neither, \
             cond_context_mgr(name, "-ambiguous.fq", bufsize, count, filt) as ambiguous:
                for chunk in fastq_chunks(fastq, bufsize=bufsize*threads, maxreads=chunkreads*threads):
                    borders = get_border(chunk[1], threads)
                    futures = [executor.submit(classify_kmers_chunkwise, chunk[0], chunk[1][borders[i]:borders[i+1]], ht) for i in range(threads)]
                    for i in as_completed(futures):
                        (classifications, linemarks) = i.result()
                        for seq in range(linemarks.shape[0]):
                            if classifications[seq] == 0:
                                host.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 1:
                                graft.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 2:
                                ambiguous.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 3:
                                both.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                counts[classifications[seq]] += 1
                            elif classifications[seq] == 4:
                                neither.write(chunk[0][linemarks[seq][2]:linemarks[seq][3]])
                                counts[classifications[seq]] += 1

                host.flush()
                graft.flush()
                ambiguous.flush()
                both.flush()
                neither.flush()

        print("counts")
        print(counts)
        return counts
    return classify_read_from_fastq
  
def make_classify_read_from_fasta(mode, shp, rcmode, bits, path, h):
    if mode == "xenome" or bits == 2:
        classify = classify_as_xenome
    elif mode == "mv":
        classify = majority_vote_classify
    elif mode == "new":
        classify = xengsort_classify
    else:
        assert()
    classify_read = make_classify_read(shp, rcmode, h)
    both = (rcmode == "both")

    # @jit
    def classify_read_from_fasta(fasta, h, skipvalue=0):
        filename, file_extension = os.path.splitext(fasta)
        if file_extension == ".gz":
            filename, file_extension = os.path.splitext(filename)
        TYPES = ['_host', '_graft', '_ambiguous', '_both', '_neither']
        files = []
        if path:
            filename = os.path.join(path, os.path.basename(filename))

        # open files
        for i in TYPES:
            files.append(open(filename + i + file_extension, "w+"))

        for header, sq in fasta_reads(fasta):
            quick_dna_to_2bits(sq)
            counts  = classify_read(h, sq)
            classification = classify(counts)
            twobits_to_dna_inplace(sq)
            str_counts = ",".join(str(i) for i in counts)
            header = ">" + header.decode("utf-8") + "counts: " + str_counts + "\n"
            files[classification].write(header)
            sq = sq.decode("utf-8") + "\n"
            files[classification].write(sq)

    return classify_read_from_fasta

def main(args):
    """ main method for classifying reads"""
    skipvalue = 0
   
    # Load hash tabel
    h, values, info = load_hash(args.index)
    
    bits = values.bits
    k = int(info['k'])
    
    rcmode = info.get('rcmode', values.RCMODE)
    if rcmode is None:  rcmode = values.RCMODE

    tsvfile = open("classification.tsv", "a+")
    now = datetime.datetime.now()
    print(f'#{now:%Y-%m-%d %H:%M:%S}: Begin classification')
    if args.fasta:
        classify_read_from_fasta = make_classify_read_from_fasta(args.classification, k, rcmode, bits, args.out, h)
        classify_read_from_fasta(f, h.hashtable, bits)
    elif args.fastq:
        if "gz" in args.prefix:
            args.prefix = ".".join(args.prefix.split('.')[:-1])
            print(args.prefix)
        if "fastq" in args.prefix or "fq" in args.prefix:
            args.prefix = ".".join(args.prefix.split('.')[:-1])
            print(args.prefix)

        if args.out[-1] =="/":
            output_folder_name = args.out + args.prefix
        else:
            output_folder_name = args.out + "/" + args.prefix

        classify_read_from_fastq = make_classify_read_from_fastq(args.classification, k, rcmode, bits, output_folder_name,
                                       args.prefix, h, args.threadcount, args.pairs, quick = args.quick, #sort=args.sort,
                                       filt=args.filter, count=args.count)
        counts = classify_read_from_fastq(args.fastq, h.hashtable)#, atsv, btsv, ntsv, htsv, gtsv)
        now = datetime.datetime.now()
        print(f'#{now:%Y-%m-%d %H:%M:%S}: All reads classified')
        print("host, graft, ambiguous, both, neither")
        str_counts = "\t".join(str(i) for i in counts)
        tsvfile.write(args.prefix + "\t" + str_counts + "\n")
        print("done")
    else:
        assert False
