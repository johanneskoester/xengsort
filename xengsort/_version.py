VERSION = "0.9"
__version__ = VERSION

DESCRIPTION = """
The xengsort package is a tool filtering xenograft data.
"""
