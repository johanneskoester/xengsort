# srindex.datasets

from os.path import join, dirname
from importlib import import_module


# Table of known maximum fill rates
# for a given number of hash functions and page size.
# Each row and column should be a growing sequence of numbers.
# (Placeholder 0.0 indicates unknown value.)
MAXFILLS = (  # for path lengths up to 4000
    #  0,   1,   2,    3,     4,    5,     6,     7,    pagesize
    (0.0,                                           ),  # hf=0
    (0.0, 0.4, 0.0, 0.00, 0.000, 0.000, 0.00, 0.000,),  # hf=1
    (0.0, 0.5, 0.0, 0.00, 0.720, 0.780, 0.78, 0.845,),  # hf=2
    (0.0, 0.9, 0.0, 0.99, 0.995, 0.996, 0.00, 0.000,),  # hf=3
)

_DATA = None


def load_datasets(fname=None):
    from json import load as load_json
    if fname is None:
        fname = join(dirname(__file__), "datasets.json")
    with open(fname, "rt") as fd:
        d = load_json(fd)
    return d


def number_of_kmers(dataset, k, *, index=0, rcmode=None):
    global _DATA
    n = -1
    if _DATA is None:
        _DATA = load_datasets()
    dk = _DATA["kmers"][dataset]
    if rcmode is not None:
        index = 1 if rcmode == "both" else 0
    if k is None:
        k = dk["k"]  # obtain a good default value
    ks = str(k)
    if ks in dk:
        n = dk[ks][index]
        found = True
        kused = k
    else:
        found = False
        kk = sorted([x for x in map(int, dk.keys()) if x>=k])
        if kk:
            kused = kk[0]
            ks = str(kused)
            n = dk[ks][index]
        else:
            kused = -1
            ks = ""
    # Guess for 'both' if number of k-mers is only known for 'fwd'
    if n < 0:
        found = False
        if index == 1 and ks:
            n = 2 * dk[ks][0]
        elif index == 0 and ks:
            n = dk[ks][1]
    if kused != k and n > 0:
        n = int(n * 1.05**abs(kused-k))  # add 5% per distance point
    return (n, found, kused)


def get_nobjects(data, k, rcmode=""):
    """
    Return pair (n, k_used) for the given data and k.
    If data is an integer, return (data, -1).
    Otherwise, interpret data as a dataset name,
    and look up the number (and a default for k, if not given).
    """
    n = 0
    try:
        n = int(data)
    except ValueError:
        pass
    if n > 0: return n, -1
    # data is the name of a dataset
    if len(data) > 2 and data[-2] == ":":
        idx = int(data[-1]) - 1
        data = data[:-2]
    else:  # use rcmode
        assert ":" not in data
        idx = 0
        if rcmode == "both": idx = 1
    (n, found, kused) = number_of_kmers(data, k, index=idx)
    return n, kused


def get_nvalues(data):
    """
    Return a pair (nvalues, rcmode) for the given 'data' (a list).
    If data has length 1 and contains a single integer,
    that number is returned, and rcmode is an empty string.
    Otherwise, data[0] is interpreted as the name of a value set,
    and data[1:] as its parameters.
    The value set is imported and the corresponding
    NVALUES and RCMODE attributes are returned.
    """
    # data is a list; data[0] may be an int
    if len(data) == 1:
        try:
            nvalues = int(data[0])
            return (nvalues, "")
        except ValueError:
            pass
    # data is a list; data[0] is the name of a value set
    vimport = "values." + data[0]
    vmodule = import_module("."+vimport, __package__)
    values = vmodule.initialize(*(data[1:]))
    return (values.NVALUES, values.RCMODE)



def get_parameters(valueset, dataset, *, rcmode=None, k=None, strict=True):
    # process valueset
    vimport = "values." + valueset[0]
    vmodule = import_module("."+vimport, __package__)
    values = vmodule.initialize(*(valueset[1:]))
    vstr =  " ".join([vimport] + valueset[1:])
    if not rcmode: rcmode = values.RCMODE

    if dataset is None:
        return (values, vstr, rcmode, k, None)

    # process dataset
    global _DATA
    ds, _, rcsuffix = dataset.partition(":")
    if _DATA is None:
        _DATA = load_datasets()
    d = _DATA["parameters"][valueset[0]][ds]
    kx = k if k is not None else d["k"]
    params = d[str(kx)]  # (htype, aligned, hashfuncs, pagesize, fill)
    (htype, aligned, hashfuncs, pagesize, fill) = params
    n, kused = get_nobjects(dataset, kx, rcmode)
    if kused != kx and k is not None and strict:
        raise RuntimeError(f"dataset {dataset}: number of objects not found for k={k}, but k={kused}")
    nfingerprints = -1  # constant, can be overridden by command line!
    parameters = (n, htype, aligned, hashfuncs, pagesize, nfingerprints, fill)
    return (values, vstr, rcmode, kx, parameters)

