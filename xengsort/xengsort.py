"""
srindex.tools.xenograft:
xenograft classification

(c) Jens Zentgraf, 2019
"""

import argparse
import os
from importlib import import_module  # dynamically import subcommand

def index(p):
    """
    Hash all (unique) k-mers of the input files,
    and store values according to the given value set.
    """
    p.add_argument("--host", "-H", metavar="HOST REFERENCE GENOME",
        help="reference file for the host organism", nargs="+", required=False)
    p.add_argument("--graft", "-G", metavar="GRAFT REFERENCE GENOME",
        help="reference file for the graft organism", nargs="+", required=False)
    p.add_argument("--dump", required=False)
    p.add_argument("index", metavar="INDEX_HDF5",
        help="name of the resulting index HDF5 file (output)")
    p.add_argument('-v', '--valueset', nargs='+', default=['xenograft', '3'],
        help="value set to use with arguments, implemented in values.{VALUESET}.py")
    p.add_argument("-d", "--dataset", metavar="DATASET",
        help="name of dataset that provides default parameters")
    p.add_argument('-k', '--kmer-size', metavar="INT", type=int,
        default="25", help="k-mer size")
    p.add_argument("--parameters", "-P", metavar="PARAMETER", nargs="+",
        help="provide parameters directly, or override dataset parameters: "
            "[NOBJECTS TYPE[:ALIGNED] HASHFUNCTIONS PAGESIZE FILL], where "
            "NOBJECTS is the number of objects to be stored, "
            "TYPE[:ALIGNED] is the hash type implemented in hash_{TYPE}.py, "
            "and ALIGNED can be a or u for aligned and unaligned, "
            "HASHFUNCTIONS is a colon-separated list of hash functions, "
            "PAGESIZE is the number of elements on a page"
            "FILL is the desired fill rate of the hash table.") 
    p.add_argument("--rcmode", metavar="MODE", default="max",
        choices=("f", "r", "both", "min", "max"),
        help="mode specifying how to encode k-mers")
    # single parameter options
    p.add_argument("-n", "--nobjects", metavar="INT", type=int,
        help="number of objects to be stored")
    p.add_argument("--type", default="3FCVbb",
        help="hash type (e.g. 3FCVbb), implemented in hash_{TYPE}.py")
    p.add_argument("--unaligned", action="store_const", 
        const=False, dest="aligned", default=None,
        help="use unaligned pages (slower, but smaller)")
    p.add_argument("--aligned", action="store_const",
        const=True, default=None,
        help="use power-of-two aligned pages (default; faster, but larger)")
    p.add_argument("--hashfunctions", "--functions", 
        help="hash functions: 'default', 'random', or func1:func2[:func3]")
    p.add_argument("--pagesize", "-p", type=int, default="6",
        help="page size, ie. number of elements on a page")
    p.add_argument("--fill", type=float, default="0.9",
        help="desired fill rate of the hash table")
    p.add_argument("--nfingerprints", type=int,
        help="override number of fingerprints (CAREFUL!)")
    # less important options
    p.add_argument("--nostatistics", "--nostats", action="store_true",
        help="do not compute or show index statistics at the end")
    p.add_argument("--longwalkstats", action="store_true",
        help="show detailed random walk length statistics")
    p.add_argument("--maxwalk", metavar="INT", type=int, default=500,
        help="maximum length of random walk through hash table before failing [500]")
    p.add_argument("--maxfailures", metavar="INT", type=int, default=0, 
        help="continue even after this many failures [default:0; forever:-1]")
    p.add_argument("--walkseed", type=int, default=7,
        help="seed for random walks while inserting elements [7]")
    p.add_argument("-C", "--chunksize", metavar="SIZE_MB",
        type=float, default=8.0,
        help="chunk size in MB [default: 8.0]. ")
    p.add_argument("-R", "--chunkreads", metavar="INT", type=int,
        help="maximum number of reads chunk per thread [SIZE_MB * 2**20 / 200]")
    p.add_argument("-B", "--blocksize", metavar="INT", type=int, default=2,
        help="elements are devided in blocks of size 4**B [default:2]")
    p.add_argument("-T", "--threadcount", metavar="INT", type=int, default=1,
        help="maximum number of threads to calculate weak kmers")
    p.add_argument("--xenome", action="store_true")

def classify(p):
    gf = p.add_mutually_exclusive_group(required=True)
    gf.add_argument("--fasta", "-f", metavar="FASTA",
        help="FASTA file to classify")
    gf.add_argument("--fastq", "-q", metavar="FASTQ",
        help="FASTQ file to classify")
    p.add_argument("--index", metavar="INDEX_HDF5",
        help="existing index file (HDF5)")
    p.add_argument("--classification", metavar="classification Mode",
        choices=("xenome", "mv", "new"), default="new",
        help="mode specifying how to classify reads")
    p.add_argument("-o", "--out" , help="output directory", default=".")
    p.add_argument("-T", "--threadcount", metavar="INT", type=int, default=1,
        help="maximum number of threads to calculate weak kmers")
    p.add_argument("--prefix", default="")
    p.add_argument("--pairs", help="paired end fastq file")
    mode = gf = p.add_mutually_exclusive_group(required=False)
#    mode.add_argument("--sort", "Sort reads in 5 classifications", action="store_true")
    mode.add_argument("--filter", help="Only output graft fq file", action="store_true")
    mode.add_argument("--count", help="Only print counts for each classification", action="store_true")
    p.add_argument("--quick", action="store_true")

##### main argument parser #############################

def get_argument_parser():
    """
    return an ArgumentParser object
    that describes the command line interface (CLI)
    of this application
    """
    p = argparse.ArgumentParser(
        description = "xenograft classification",
        epilog = "by Genome Informatics, University of Duisburg-Essen."
        )
    
    subcommands = [
        ("index",
         "build an index for host and graft reference gneomes",
         index,
         "index", "main"),
         ("classify",
         "classify reads to host or graft genome",
         classify,
         "classify", "main")
    ]
    # add global options here
#    p.add_argument("--version", action="version", version=VERSION)
    # add subcommands to parser
    sps = p.add_subparsers(
        description="The srindex library supports the following commands.",
        metavar="COMMAND")
    sps.required = True
    sps.dest = 'subcommand'
    for (name, helptext, f_parser, module, f_main) in subcommands:
        if name.endswith('!'):
            name = name[:-1]
            chandler = 'resolve'
        else:
            chandler = 'error'
        sp = sps.add_parser(name, help=helptext,
            description=f_parser.__doc__, conflict_handler=chandler)
        sp.set_defaults(func=(module,f_main))
        f_parser(sp)
    return p

def main(args=None):
    p = get_argument_parser()
    pargs = p.parse_args() if args is None else p.parse_args(args)
    (module, f_main) = pargs.func
    os.environ["OMP_NUM_THREADS"] = str(pargs.threadcount)
    os.environ["OPENBLAS_NUM_THREADS"] = str(pargs.threadcount)
    os.environ["MKL_NUM_THREADS"] = str(pargs.threadcount)
    os.environ["VECLIB_MAXIMUM_THREADS"] = str(pargs.threadcount)
    os.environ["NUMEXPR_NUM_THREADS"] = str(pargs.threadcount)
    os.environ["NUMBA_NUM_THREADS"] = str(pargs.threadcount)
    m = import_module("."+module, __package__)
    mymain = getattr(m, f_main)
    mymain(pargs)
