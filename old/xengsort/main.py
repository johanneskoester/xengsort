"""
xengsort.main:
parse the command line arguments and execute the appropriate submodule

This file defines the command line interfaces (CLIs)
for all subcommands of the xengsort tool.

(c) Sven Rahmann, 2017
"""

import argparse
from importlib import import_module  # to dynamically import subcommand

from .version import VERSION, DESCRIPTION


# subparsers for all subcommands
# (for global structure, see get_argument_parser function below)


# some default parameters for the 'altindex' subcommand

# 3-level human-mouse design with 3.21836 bytes/25mer
L3_K25 = "choice,swapslide:slide,29,4; choice,swapslide:slide,26,7; choice,swapslide:slide,23,6"
# Alternative human-mouse design with BAD bytes/25mer
A_K25 = "choice,slide:swapslide,30,2; choice,swapslide:slide,28,2; choice,slide:swapslide,25,3"  # BAD
# 4-level human-mouse design with 3.17482 bytes/25mer so far 8 overflows for k=25, 1.777 Mio for k=23
L4_K23 = "choice,swapslide:slide,29,4; choice,swapslide:slide,26,7; choice,slide:swapslide,24,3; choice,swapslide:slide,18,6"
L4_K25 = "choice,swapslide:slide,29,4; choice,swapslide:slide,27,3; choice,swapslide:slide,24,5; choice,swapslide:slide,21,6"
DEFAULT = (25, L3_K25)

def index_alternative(p):
    """
    Build a k-mer index of two genomes (graft and host) in order to later
    sort FASTQ reads into their originating genomes
    """
    p.add_argument("index", metavar="INDEX_HDF5",
        help="file name of the resulting hdf5 index file")
    p.add_argument("--graft", "-g", metavar="GRAFT_FASTA", nargs="+",
        help="FASTA file(s) of the graft genome (e.g. human)")
    p.add_argument("--host", "-h", metavar="HOST_FASTA", nargs="+",
        help="FASTA file(s) of the host genome (e.g. mouse)")
    p.add_argument('--graftname', '-G', metavar="STRING", default="graft",
        help="Name of graft genome [graft], e.g. human")
    p.add_argument('--hostname', '-H', metavar="STRING", default="host",
        help="Name of host genome [host], e.g. mouse")
    p.add_argument('-k', '--kmer-size', metavar="INT",
        type=int, default=DEFAULT[0],
        help="k-mer size [25]")
    p.add_argument("--parameters", "--params", "-p", metavar="PARAMETERS",
       default=DEFAULT[1],
       help="hash parameters: type1,func1a[:func1b],p1,s1[,f1]; type2,func2,p2,s2[,f2];... ")


def index(p):
    """
    Build a k-mer index of two genomes (graft and host) in order to later
    sort FASTQ reads into their originating genomes
    """
    p.add_argument("index", metavar="INDEX_HDF5",
        help="file name of the resulting hdf5 index file")
    p.add_argument("--graft", "-g", metavar="GRAFT_FASTA", nargs="+",
        help="FASTA file(s) of the graft genome (e.g. human)")
    p.add_argument("--host", "-h", metavar="HOST_FASTA", nargs="+",
        help="FASTA file(s) of the host genome (e.g. mouse)")
    p.add_argument('--graftname', '-G', metavar="STRING", default="graft",
        help="name of graft genome [graft], e.g. human")
    p.add_argument('--hostname', '-H', metavar="STRING", default="host",
        help="name of host genome [host], e.g. mouse")
    p.add_argument('-k', '--kmer-size', metavar="INT",
        type=int, default=25, help="k-mer size [k=25 is 50 bits]")
    p.add_argument('-p', '--pagebits', metavar="INT", type=int, default=28,
        help="number of bits of a page address; there are 2**pagebits pages.")
    p.add_argument('-r', '--samplerate', '--rate', type=int, default=128,
        help="sampling rate of cumulative counters [128]")
    p.add_argument('-b', '--samplebits', '--bits', type=int, 
        choices=(32,64), default=32,
        help="number of bits for cumulative counters (default 32, or 64)")
    p.add_argument('-f', '--fingerprintbits', '--fprbits', metavar="INT",
        type=int, default=-1,
        help="number of fingerprint bits [2*q - pagebits]")
    p.add_argument("--hashfunction", metavar="NAME", default="swapslide",
        help="name of hash function [swapslide]")


def sort(p):
    """
    Sort FASTQ reads into graft and host genome.
    """
    pass


##### main argument parser #############################

def get_argument_parser():
    """
    return an ArgumentParser object
    that describes the command line interface (CLI)
    of this application
    """
    p = argparse.ArgumentParser(
        description = "xengsort: xenograft sequence sorting",
        epilog = "by Genome Informatics, University of Duisburg-Essen."
        )
    # define subcommands: (command_name, help, parser_function, module_name, main_name)
    subcommands = [
        ("index!",  # ! allows overwriting existing options (-h) for this command
         "index two genomes or transcriptomes (graft, host) by hashing their k-mers",
         index,
         "index", "main"),
        ("altindex!",  # ! allows overwriting existing options (-h) for this command
         "index two genomes or transcriptomes (graft, host) by hashing their k-mers",
         index_alternative,
         "index", "main_alternative"),
        ("sort",
         "sort FASTQ reads from xenograft samples into originating genomes",
         sort,
         "sort", "main"),
        ]
    # add global options here
    p.add_argument("--version", action="version", version=VERSION)
    # add subcommands to parser
    sps = p.add_subparsers(
        description="The xengsort application is divided into the following commands.",
        metavar="COMMAND")
    sps.required = True
    sps.dest = 'subcommand'
    for (name, helptext, f_parser, module, f_main) in subcommands:
        if name.endswith('!'):
            name = name[:-1]
            chandler = 'resolve'
        else:
            chandler = 'error'
        sp = sps.add_parser(name, help=helptext,
            description=f_parser.__doc__, conflict_handler=chandler)
        sp.set_defaults(func=(module,f_main))
        f_parser(sp)
    return p


def main(args=None):
    p = get_argument_parser()
    pargs = p.parse_args() if args is None else p.parse_args(args)
    (module, f_main) = pargs.func
    # import the appropriate module and call function f_main with pargs
    m = import_module("."+module, __package__)
    mymain = getattr(m, f_main)
    mymain(pargs)

if __name__ == "__main__":
    main()
