import sys
import io
import gzip
from collections import Counter
from subprocess import check_output
from os import path as ospath


# FASTA/FASTQ/TEXT/gz handling ######################################

class FormatError(RuntimeError):
    pass


def fastq_reads(files, sequences_only=False):
    """
    For the given 
    - list or tuple of FASTQ paths,
    - single FASTQ path (string; "-" for stdin)
    - open binary FASTQ file-like object f,
    yield a triple of bytes (header, sequence, qualities) for each read.
    If sequences_only=True, yield only the sequence of each read.

    This function operatates at the bytes (not string) level.
    The header contains the initial b'@' character.

    Automatic gzip decompression is provided
    if a file is a string and ends with .gz or .gzip.
    """
    func = _fastq_reads_from_filelike if not sequences_only else _fastq_seqs_from_filelike
    if type(files) == list or type(files) == tuple:
        # multiple files
        for f in files:
            yield from _universal_reads(f, func)
    else:
        # single file
        yield from _universal_reads(files, func)


def fasta_reads(files, sequences_only=False):
    """
    For the given
    - list or tuple of FASTA paths,
    - single FASTA path (string),
    - open binary FASTA file-like object f,
    yield a pair of bytes (header, sequence) for each entry (of each file).
    If sequences_only=True, yield only the sequence of each entry.
    This function operatates at the bytes (not string) level.
    The header DOES NOT contain the initial b'>' character.
    If f == "-", the stdin buffer is used.
    Automatic gzip decompression is provided,
    if f is a string and ends with .gz or .gzip.
    """
    func = _fasta_reads_from_filelike if not sequences_only else _fasta_seqs_from_filelike
    if type(files) == list or type(files) == tuple:
        # multiple files
        for f in files:
            yield from _universal_reads(f, func)
    else:
        # single file
        yield from _universal_reads(files, func)


def text_reads(f):
    """
    For the given text file path or open binary file-like object f,
    yield a each line as a bytes object, without the newline.
    If f == "-", the stdin buffer is used.
    Automatic gzip decompression is provided
    if f is a string and ends with .gz or .gzip.
    """
    yield from _universal_reads(f, _text_reads_from_filelike)


####################################

def _universal_reads(f, func):
    """
    yield each read from file f, where
    f is a filename (string), possibly ending with .gz/.gzip,
      or a file-like object,
    func describes the logic of obtaining reads from the file,
    and can be one of
      _fastq_reads_from_filelike: yields triples (header, sequence, qualities)
      _fastq_seqs_from_filelike: yields sequences only
      _fasta_reads_from_filelike: yields pairs (header, sequence)
      _fasta_seqs_from_filelike: yields sequences only
      _text_reads_from_filelike: yields each line as a read
    All objects are bytes/bytearray objects (can be decoded using ASCII encoding).
    Headers are yielded WITHOUT the initial character (> for FASTA, @ for FASTQ).
    """
    if not isinstance(f, str) and not isinstance(f, bytes):
        yield from func(f)
    elif f == "-" or f == b"-":
        yield from func(sys.stdin.buffer)
    elif f.endswith((".gz", ".gzip")):
        with gzip.open(f, "rb") as file:
            reader = io.BufferedReader(file, 4*1024*1024)
            yield from func(reader)
    else:
        with open(f, "rb", buffering=-1) as file:
            yield from func(file)


def _fastq_reads_from_filelike(f, HEADER=b'@'[0], PLUS=b'+'[0]):
    strip = bytes.strip
    entry = 0
    while f:
        header = strip(next(f))
        if not header: continue
        entry += 1
        seq = strip(next(f))
        plus = strip(next(f))
        qual = strip(next(f))
        if header[0] != HEADER:
            raise FormatError("ERROR: Illegal FASTQ header: '{}', entry {}".format(
                header.decode(), entry))
        if plus[0] != PLUS:
            raise FormatError("ERROR: Illegal FASTQ plus line: '{}',\nlast header was '{}',\nsequence was '{}',\nentry {}".format(plus.decode(), header.decode(), seq.decode(), entry))
        if len(plus) > 1 and plus[1:] != header[1:]:
            raise FormatError("ERROR at: Header/plus mismatch: '{}' vs. '{}', entry {}".format(
                header.decode(), plus.decode(), entry))
        yield (header[1:], seq, qual)


def _fastq_seqs_from_filelike(f, HEADER=b'@'[0], PLUS=b'+'[0]):
    strip = bytes.strip
    while f:
        header = strip(next(f))
        if not header: continue
        seq = strip(next(f))
        plus = next(f)
        next(f)  # ignore quality value
        if header[0] != HEADER:
            raise FormatError("ERROR: Illegal FASTQ header: {}".format(header.decode()))
        if plus[0] != PLUS:
            raise FormatError("ERROR: Illegal FASTQ plus line: {}".format(plus.decode()))
        yield seq


def _fasta_reads_from_filelike(f, COMMENT=b';'[0], HEADER=b'>'[0]):
    strip = bytes.strip
    header = seq = None
    for line in f:
        line = strip(line)
        if len(line) == 0:
            continue
        if line[0] == COMMENT:
            continue
        if line[0] == HEADER:
            if header is not None:
                yield (header, seq)
            header = line[1:]
            seq = bytearray()
            continue
        seq.extend(line)
    if header is not None:
        yield (header, seq)


def _fasta_seqs_from_filelike(f, COMMENT=b';'[0], HEADER=b'>'[0]):
    strip = bytes.strip
    header = seq = False
    for line in f:
        line = strip(line)
        if len(line) == 0:
            continue
        if line[0] == COMMENT:
            continue
        if line[0] == HEADER:
            if header:
                yield seq
            header = True
            seq = bytearray()
            continue
        seq.extend(line)
    yield seq


def _text_reads_from_filelike(f):
    strip = bytes.strip
    while f:
        yield strip(next(f))


####################################################################

# Grouping
#
# A groupby function is a function that returns a group name, given
# - a filename (full absolute path or relative path)
# - a sequence name (header string)


def groupby_all(path, header):
    return "all"


def groupby_basename(path, header):
    fname = ospath.basename(ospath.abspath(path))
    while True:
        fname, ext = ospath.splitext(fname)
        if not ext:
            break
    return fname


def groupby_seqname(path, header):
    fields = header[1:].split()
    if not fields:
        return "default"
    return fields[0]


def groupby_seqname_strict(path, header):
    return header[1:].split()[0]  # raises IndexError if header is empty


def groupby_dict_factory(d, default="default"):
    """
    return a groupby function that looks up group in given dict,
    using the first word of the sequence header
    """
    def groupby_d(path, header):
        fields = header[1:].split()
        name = fields[0] if fields else ''
        if default:
            return d.get(name, default)
        return d[name]
    return groupby_d


def get_grouper(groupspec):
    """
    groupspec is singleton list or pair list:
    (method[, specifier]) with the following possibilities:
    ('const', constant): a constant group name for all sequences
    """
    method = groupspec[0]
    spec = groupspec[1] if len(groupspec) > 1 else None
    if method == 'const':
        if spec is None:
            raise ValueError('groupby "const" needs an argument (the constant)')
        return lambda path, header: spec
    if method == 'all':
        return None
    raise NotImplementedError('this groupby functionality is not yet implemented: {}'.format(groupspec))


def get_group_sizes(files, groupmap, offset=0, override=None):
    lengths = Counter()
    if files is None:
        return lengths
    if override is not None:
        if groupmap is None:
            lengths["all"] = override
            return lengths
        for (_, _, group) in grouped_sequences(files, groupmap):
            lengths[group] = override
        return lengths
    # count total lengths of sequences
    for (_, seq, group) in grouped_sequences(files, groupmap):
        lengths[group] += len(seq) + offset
    return lengths


def grouped_sequences(files, groupby=None, format=None):
    """
    For each sequence in the given list/tuple of files or (single) file path,
    yield a triple (header, sequence, group),
    according to the given groupby function.

    The file format (.fast[aq][.gz]) is recognized automatically,
    but can be explicitly given by format="fasta" or format="fastq".
    """
    if type(files) == list or type(files) == tuple:
        for f in files:
            yield from _grouped_sequences_from_a_file(f, groupby, format=format)
    else:
        yield from _grouped_sequences_from_a_file(files, groupby, format=format)


def _grouped_sequences_from_a_file(fname, groupby=None, format=None):
    few = fname.lower().endswith
    if format is not None:
        format = format.lower()
    if format == "fasta" or few((".fa", ".fna", ".fasta", ".fa.gz", ".fna.gz", ".fasta.gz")):
        if groupby is not None:
            reads = _fasta_reads_from_filelike
            for (h, s) in _universal_reads(fname, reads):
                g = groupby(fname, h)
                yield (h, s, g)
        else:
            reads = _fasta_seqs_from_filelike
            for s in _universal_reads(fname, reads):
                yield (True, s, "all")
    elif format == "fastq" or few((".fq", ".fastq", ".fq.gz", ".fastq.gz")):
        if groupby is not None:
            reads = _fastq_reads_from_filelike
            for (h, s, q) in _universal_reads(fname, reads):
                g = groupby(fname, h)
                yield (h, s, g)
        else:
            reads = _fastq_seqs_from_filelike
            for s in _universal_reads(fname, reads):
                yield (True, s, "all")
    else:
        raise FormatError("format of file '{}' not recognized".format(fname))


####################################################################

def get_sizebounds(files):
    """
    return a pair (sumbound, maxbound), where
    sumbound is an upper bound on the sum of the number of q-grams in the given 'files',
    maxbound is an upper bound on the maximum of the number of q-grams in one entry in 'files'.
    """
    if files is None:
        return (0, 0)
    sb = mb = 0
    for (_, seq, _) in grouped_sequences(files):
        ls = len(seq)
        sb += ls
        if ls > mb:  mb = ls
    return (sb, mb)


def number_of_sequences_in(fname):
    # TODO: this only works with Linux / Mac
    few = fname.lower().endswith
    if few((".fa", ".fasta")):
        x = check_output(["grep", "-c", "'^>'", fname])
        return int(x)
    if few((".fa.gz", ".fasta.gz")):
        x = check_output(["gzcat", fname, "|", "grep", "-c", "'^>'"])
        return int(x)
    if few((".fq", ".fastq")):
        x = check_output(["wc", "-l", fname])
        n = int(x.strip().split()[0])
        # if n%4 != 0:
        #     raise RuntimeError("number of lines [{}] not divisible by 4".format(n))
        return n//4
    if few((".fq.gz", ".fastq.gz")):
        x = check_output(["gzcat", fname, "|", "wc", "-l"])
        n = int(x.strip().split()[0])
        # if n%4 != 0:
        #     raise RuntimeError("number of lines [{}] not divisible by 4".format(n))
        return n//4


# FASTQ checking ####################################################

def fastqcheck(args):
    files = args.sequences
    if args.paired:
        success = fastqcheck_paired(list(zip(*[iter(files)]*2)), args)
    else:
        success = fastqcheck_single(files, args)
    exitcode = 0 if success else 1
    sys.exit(exitcode)


def fastqcheck_paired(filepairs, args):
    success = True
    for (f1, f2) in filepairs:
        print("Checking {}, {}...".format(f1, f2))
        msg = "OK"
        try:
            for entry1, entry2 in zip(fastq_reads(f1), fastq_reads(f2)):
                c1 = entry1[0].split()[0]
                c2 = entry2[0].split()[0]
                if c1 != c2:
                    raise FormatError("headers {} and {} do not match".format(c1.decode(),c2.decode()))
        except FormatError as err:
            success = False
            msg = "FAILED: " + str(err)
        print("{}, {}: {}".format(f1, f2, msg))
    return success


def fastqcheck_single(files, args):
    success = True
    for f in files:
        print("Checking {}...".format(f))
        msg = "OK"
        try:
            for entry in fastq_reads(f):
                pass
        except FormatError as err:
            success = False
            msg = "FAILED: " + str(err)
        print("{}: {}".format(f, msg))
    return success


# FASTA header extraction ###########################################

_SEPARATORS = {'TAB': '\t', 'SPACE': ' '}

def fastaextract(args):
    """extract information from FASTA headers and write in tabular form to stdout"""
    files = args.files
    items = args.items
    seps = args.separators
    sfx = [args.suffix] if args.suffix else []
    seps = [_SEPARATORS.get(sep.upper(), sep) for sep in seps]
    if items is None: 
        items = list()
        seps = list()
    if len(seps) == 1:
        seps = seps * len(items)
    seps = [""] + seps
    head = ['transcript_id'] + items

    first = [x for t in zip(seps, head) for x in t] + sfx
    print("".join(first))
    for f in files:
        for (header, _) in fasta_reads(f):
            infolist = get_header_info(header, items, ":", seps) + sfx
            print("".join(infolist))


def get_header_info(header, items, assigner, seps):
    fields = header.decode("ascii").split()
    assigners = [i for (i,field) in enumerate(fields) if assigner in field]
    if 0 in assigners: 
        assigners.remove(0)
    D = dict()
    if items is None: items = list()
    for j, i in enumerate(assigners):
        field = fields[i]
        pos = field.find(assigner)
        assert pos >= 0
        name = field[:pos]
        nexti = assigners[j+1] if j+1 < len(assigners) else len(fields)
        suffix = "_".join(fields[i+1:nexti])
        if len(suffix)==0:
            D[name] = field[pos+1:]
        else:
            D[name] = field[pos+1:] + '_' + suffix
    # dictionary D now has values for all fields
    L = [seps[0], fields[0]]
    for i, item in enumerate(items):
        if item in D:
            L.append(seps[i+1])
            L.append(D[item])
    return L


# FASTQ name guessing #######################################

def guess_pairs(fastq, replace):
    if replace != 1 and replace != 2:
        raise ValueError("Replace must be integer 1 or 2")
    # replace=1: Replace rightmost occurrence of '_R2' by '_R1'
    # replace=2: Replace rightmost occurrence of '_R1' by '_R2'
    orig = str(3 - replace)
    o = "_R" + orig
    r = "_R" + str(replace)
    pairnames = list()
    for name in fastq:
        start = name.rfind(o)
        if start < 0:
            raise ValueError("FASTQ file name '{}' does not contain '{}'".format(name, o))
        pairnames.append(name[:start] + r + name[start+len(o):])
    return pairnames

