"""
Definition of the final hash table with linear scanning
"""

from math import ceil
from collections import namedtuple

import numpy as np
from numba import njit, int64

from .dnaencode import generate_revcomp_and_canonical_code
from .generalhash import generate_get_page_and_get_fingerprint, print_info


FinalHash = namedtuple("FinalHash",
    ["hashtype",
     "hashtable",
     "get_page",
     "get_fingerprint",
     "store_code",
     "get_genome_for_code",
     "get_occupancy",
     "memorybits",
    ])


def generate_final_hash(q, pagebits, fprbits, pagesize, hf):
    """
    Allocate an array and compile access methods for a hash table.
    
    Parameters:
    q:        q-gram length; all fingerprints are based on q-grams (2q bits)
    pagebits: number of bits for indexing a hash table page;  pagebits < 2*q
    fprbits:  number of bits for fingerprints;  pagebits + fprbits <= 2*q
    pagesize: number of slots in one hash page; the total hash table size is
              2**pagebits * pagesize * ceil((fprbits+2) / 8) bytes
    hf:       name of hash function

    Return a ChoiceHash namedtuple instance.
    """
    qbits = 2*q
    if fprbits < 0:
        fprbits = qbits
    if fprbits != qbits:
        raise ValueError("must have fprbits == qbits for the final hash")
    if fprbits > 62:
        raise ValueError("must have fprbits <= 62")
    slotbits = fprbits + 2
    assert slotbits <= 64
    pagesizebits = slotbits * pagesize  # e.g. 64 for fprbits=14, pagesize=4  

    tablebits = 2**pagebits * pagesizebits
    tablewords = int(ceil(tablebits / 64))
    hashtable = np.zeros(tablewords, dtype=np.int64)
    print_info("FinalHash", slotbits, pagesize, pagesizebits, pagebits, tablewords)
    
    # structure of an item in a slot:  ---ggffffffff
    # gg: two genome bits
    # fffffff: fingerprint bits
    pagemask = 2**pagebits - 1
    fprmask = 2**fprbits - 1
    
    # define get_page(code) and get_fingerprint(code) functions
    if not hf:
        hf = 'final'
    if not hf.startswith('final'):
        raise ValueError("hash function for FinalHash must start with final")
    (get_page, get_fingerprint) = generate_get_page_and_get_fingerprint(
            hf, q, pagebits, fprbits)

    # Extract the item on a given page in a given slot
    @njit(locals=dict(x=int64, mask1=int64, a=int64, b=int64, b1=int64))
    def get_item(table, page, slot):
        """
        Return the full item x from the given (table, page, slot).
        The bits x=(gg|f...f) must be separated to get genome, fingerprint.
        """
        # must have 0 <= page < 2**pagebits and 0 <= slot < pagesize
        startbit = page * pagesizebits + slot * slotbits
        a = startbit // 64  # item starts in table[a]
        b = startbit & 63   # at bit number b
        if b + slotbits <= 64:  # get slotbits bits
            # bits are contained in a single int64
            x = table[a] >> b
        else:
            # bits are distributed over two int64s, 
            # leftmost b1 = 64-b in table[a], rightmost b2 = slotbits-b1 in table[a+1]
            b1 = 64 - b
            mask1 = 2**b1 - 1
            x = (table[a] >> b) & mask1 
            x |= (table[a+1] << b1)
        return x

    @njit(locals=dict(page=int64, slot=int64, f=int64, p=int64, g=int64,
              v=int64, v1=int64, startbit=int64, a=int64, 
              b=int64, b1=int64, b2=int64, mask1=int64, mask2=int64))
    def set_item(table, page, slot, f, g):
        """
        store the item x from fingerprint f, pagechoice p, genome g
        at the given table, page, slot combination,
        overwriting what was there previously without checking.
        """
        # set fingerprint/pagechoice/genome bits to f/p/g, respectively
        v = (f & fprmask) | ((g&3) << fprbits)
        startbit = page * pagesizebits + slot * slotbits
        a = startbit // 64
        b = startbit & 63
        if b + slotbits <= 64:
            # bits are contained in a single int64
            mask1 = ~((2**slotbits - 1) << b)
            table[a] = (table[a] & mask1) | (v << b)
        else:
            # b1 leftmost bits in table[a] = b1 rightmost bits of v, 
            b1 = 64 - b  # b1 leftmost bits in table[a]
            mask1 = (2**b) - 1  # only keep b rightmost bits
            v1 = (v & (2**b1 - 1))
            table[a] = (table[a] & mask1) | (v1 << b)
            # b2 rightmost bits in table[a+1] = b2 leftmost bits of v
            b2 = slotbits - b1
            mask2 = ~(2**b2 - 1)
            table[a+1] = (table[a+1] & mask2) | (v >> b1)

    @njit(locals=dict(fpr=int64, page=int64, page0=int64,
            slot=int64, x=int64, g=int64, f=int64))
    def _get_pagestatus(table, fpr, page):
        # return (genome, page, slot) if fpr found 
        # return (0, page, slot) if not found and (page, slot) is free
        # return (-1, -1, -1) if not found and no free slot found
        page0 = page
        while True:
            for slot in range(pagesize):
                x = get_item(table, page, slot)
                g = (x >> fprbits) & 3
                if g == 0:
                    return (0, page, slot)
                f = x & fprmask
                if f == fpr:
                    return (g, page, slot)
            page = (page + 1) & pagemask
            if page == page0:  break
        return (-1, -1, -1)
    
    @njit(locals=dict(code=int64, base=int64, slot=int64, x=int64, f=int64, g=int64))
    def store_code(code, genome, table):
        """
        Attempt to store given code with given genome in hash table.
        Return values:
        -2: Code and genome were already present
        -1: Code was present, new genome information was added
         0: New code was added with given genome information
         1: Total overflow, error
        """
        page = get_page(code)
        fpr = get_fingerprint(code)
        (g, page, slot) = _get_pagestatus(table, fpr, page)
        if g > 0:  # already found at (page, slot)
            newgenome = (g|genome) != g
            if newgenome:
                set_item(table, page, slot, fpr, (g|genome))
                return -1
            else:
                return -2
        if g == 0:  # store new
            set_item(table, page, slot, fpr, genome)
            return 0
        return 1

    
    @njit(locals=dict(code=int64, page=int64, fpr=int64, g=int64))
    def get_genome_for_code(code, table):
        """
        return genome information (0,1,2,3) for given code,
        or -1 if all is full
        """
        page = get_page(code)
        fpr = get_fingerprint(code)
        (g, _, _) = _get_pagestatus(table, fpr, page)
        return g


    @njit(locals=dict(page=int64, last=int64, slot=int64, x=int64, g=int64))
    def get_occupancy(table):
        pagefill = np.zeros(pagesize+1, dtype=np.int64)
        genomefill = np.zeros(4, dtype=np.int64)
        for page in range(2**pagebits):
            last = -1
            for slot in range(pagesize):
                x = get_item(table, page, slot)
                g = (x >> fprbits) & 3
                genomefill[g] += 1
                if g != 0:
                    last = slot
            pagefill[last+1] += 1
        return pagefill, genomefill


    return FinalHash(
        hashtype="final",
        hashtable=hashtable,
        get_page=get_page,
        get_fingerprint=get_fingerprint,
        store_code=store_code,
        get_genome_for_code=get_genome_for_code,
        get_occupancy=get_occupancy,
        memorybits=tablebits,
        )

# end of file