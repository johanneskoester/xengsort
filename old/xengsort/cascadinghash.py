"""
Definition of Cascading Hash Tables
"""

from collections import namedtuple

import numpy as np
from numba import njit, int64

#from .h5utils import load_from_h5group, save_to_h5group
from .dnaencode import generate_revcomp_and_canonical_code
from .singlehash import generate_single_hash
from .choicehash import generate_choice_hash
from .finalhash import generate_final_hash
_HASHTYPES = {'single', 'choice', 'final'}

CascadingHash = namedtuple("CascadingHash",
    ["hashtype",    # type of component hashes
     "levels",      # number of levels
     "components",  # a levels-tuple of SingleHash objects
     "hashtables",  # a (levels+1)-tuple of the hashtables of components plus overflow buffer
     "overflow",    # int64 buffer for overflow storage
     "store_code",  # compiled function to store a canoical code in cascading tables
     "hash_qgrams", # compiled function to hash q-grams of a sequence
     "count_valid_qgrams",  # compiled function to count valid q-grams in a sequence
     "get_occupancy",  # function to get occupancy statistics
    ])


def generate_cascading_hash(q, hashtypes, hashfuncs, pagebits, pagesizes, fprbits, overflowsize):
    """
    Create cascading hash tables and compile their access methods.
    Return a CascadingHash instance.
    
    Parameters should be n-tuples (except q) for an n-level hash.
    q:         q-gram length; all fingerprints are based on q-grams (2q bits) of the sequences
    hashtypes: n-tuple of hash types from {"simple", "choice", "end"}
    hashfuncs: n-tuple of hash functions from those in generalhash.py
    pagebits:  n-tuple of page bits  for the single hashes
    pagesizes: n-tuple of page sizes for the single hashes
    fprbits:   n-tuple of fingerprint bits for the single hashes
    overflowsize: capacity of the emergency overflow buffer

    """
    if q < 0 or q > 31:
        raise ValueError("only 1 <= q <= 31 is supported")
    levels = len(hashtypes)
    if (len(hashfuncs) != levels or len(pagebits) != levels 
        or len(pagesizes) != levels or len(fprbits) != levels):
        raise ValueError("hashtypes, hashfuncs, pagebits, pagesizes and fprbits have the same length (#levels)")
    for hashtype in hashtypes:
        if hashtype not in _HASHTYPES:
            raise ValueError("hashtype must be in "+str(_HASHTYPES))
    # create all components, the single hashes
    components = list()
    for i in range(levels):
        ht = hashtypes[i]
        hf = hashfuncs[i]
        if ht == "choice":
            if not isinstance(hf, tuple):
                raise TypeError("for hashtype 'choice', hashfunc must be 'func1:func2'")
            ci = generate_choice_hash(q, pagebits[i], fprbits[i], pagesizes[i], hf[0], hf[1])
        elif ht == "single":
            if not isinstance(hf, str):
                raise TypeError("for hashtype 'single', hashfunc must be a single string")
            ci = generate_single_hash(q, pagebits[i], fprbits[i], pagesizes[i], hf)
        elif ht == "final":
            if not isinstance(hf, str):
                raise TypeError("for hashtype 'final', hashfunc must be a single string")
            ci = generate_final_hash(q, pagebits[i], fprbits[i], pagesizes[i], hf)
        else:
            raise NotImplementedError("type '{}' not implemented".format(ht))
        components.append(ci)
    components = tuple(components)
    
    # define the function 'store_code' 
    # that stores a given code in the hash cascade
    # TODO:  ugly code because numba does not deal well
    # with a tuple of functions in the enclosing scope,
    # so we need separate definitions depending on levels :(
    if levels == 1:
        storecode0 = components[0].store_code
        @njit(locals=dict(code=int64, genome=int64, result=int64))
        def store_code(code, genome, tables):
            """
            Attempt to store the code and genome information in each component.
            Return values:
            -2:  Code and genome were already present
            -1:  Code was present, new genome information was added
            0..levels-1: New code was added with given genome information
            levels: Overflow; code was not stored
            """
            result = storecode0(code, genome, tables[0])
            if result <= 0:
                return result
            return 1
    elif levels == 2:
        storecode0 = components[0].store_code
        storecode1 = components[1].store_code
        @njit(locals=dict(code=int64, genome=int64, result=int64))
        def store_code(code, genome, tables):
            # level 0
            result = storecode0(code, genome, tables[0])
            if result <= 0:
                return result
            # level 1
            result = storecode1(code, genome, tables[1])
            if result < 0:
                return result
            if result == 0:
                return 1
            return 2
    elif levels == 3:
        storecode0 = components[0].store_code
        storecode1 = components[1].store_code
        storecode2 = components[2].store_code
        @njit(locals=dict(code=int64, genome=int64, result=int64))
        def store_code(code, genome, tables):
            # level 0
            result = storecode0(code, genome, tables[0])
            if result <= 0:
                return result
            # level 1
            result = storecode1(code, genome, tables[1])
            if result < 0:
                return result
            if result == 0:
                return 1
            # level 2
            result = storecode2(code, genome, tables[2])
            if result < 0:
                return result
            if result == 0:
                return 2
            return 3
    elif levels == 4:
        (storecode0, storecode1, storecode2, storecode3) = tuple(components[i].store_code for i in range(levels))
        @njit(locals=dict(code=int64, genome=int64, result=int64))
        def store_code(code, genome, tables):
            # level 0
            result = storecode0(code, genome, tables[0])
            if result <= 0:
                return result
            # level 1
            result = storecode1(code, genome, tables[1])
            if result < 0:
                return result
            if result == 0:
                return 1
            # level 2
            result = storecode2(code, genome, tables[2])
            if result < 0:
                return result
            if result == 0:
                return 2
            # level 3
            result = storecode3(code, genome, tables[3])
            if result < 0:
                return result
            if result == 0:
                return 3
            return 4
    else:
        raise NotImplementedError("{} levels not currently supported".format(levels))
    
    shp = tuple(range(q))
    revcomp_code, canonical_code = generate_revcomp_and_canonical_code(q)
    
    @njit(locals=dict(start=int64, end=int64, genome=int64, over=int64,
            valid=int64, startpoints=int64, i=int64, j=int64,
            code=int64, c=int64, result=int64,))
    def hash_qgrams(seq, start, end, genome, tables, overflowtable, over):
        """
        hash valid q-grams into cascading hash tables
        dvalid, over = hash_qgrams(sq, 0, len(sq), genome, ht, ot, over)
        """
        valid = 0
        startpoints = (end - start) - shp[q-1]
        for i in range(start, start+startpoints):
            code = 0
            for j in shp:
                c = seq[i+j]
                if c > 3:
                    break
                code = (code << 2) + c
            else:  # code is valid, no break
                valid += 1
                code = canonical_code(code)
                result = store_code(code, genome, tables)
                if result >= levels:
                    if over >= overflowtable.size:
                        return (valid, over+1)
                    overflowtable[over] = code | (genome << (2*q))
                    over += 1
        return (valid, over)
    
    # A convenience function that counts valid q-grams
    @njit(locals=dict(code=int64, c=int64, valid=int64, startpoints=int64))
    def count_valid_qgrams(seq, start, end):
        """
        return number of valid q-grams in seq[start:end],
        where a q-gram seq[i:i+q] in seq is valid if all elements are in 0,1,2,3.
        """
        valid = 0
        startpoints = (end - start) - shp[q-1]
        for i in range(start, start+startpoints):
            code = 0
            for j in shp:
                c = seq[i+j]
                if c > 3:
                    break
                code = (code << 2) + c
            else:  # code is valid, no break
                valid += 1
        return valid

    # not jitted!
    def get_occupancy(tables, overflowtable):
        pocc = list()
        gocc = np.zeros(4, dtype=np.int64)
        # len(tables) == levels+1
        for level in range(levels):
            pageocc, genomeocc = components[level].get_occupancy(tables[level])
            gocc += genomeocc
            pocc.append(pageocc)
        # TODO: deal with overflowtable!
        return pocc, gocc


    overflow = np.zeros(overflowsize, dtype=np.int64)
    hashtables = tuple(c.hashtable for c in components)

    ch = CascadingHash(
        hashtype=hashtype,
        levels=levels,
        components=components,
        hashtables=hashtables,
        overflow=overflow,
        store_code=store_code,
        hash_qgrams=hash_qgrams,
        count_valid_qgrams=count_valid_qgrams,
        get_occupancy=get_occupancy)

    return ch
