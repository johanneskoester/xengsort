
import numpy as np
from numba import njit, int64

#from .h5utils import load_from_h5group, save_to_h5group
from .dnaio import fastq_reads
from .dnaencode import dna_to_2bits


def main(args):
    """main method for read sorting"""
    # x sort -i idx.h5 --first a1.fq b1.fq c1.fq --second a2.fq b2.fq c2.fq
    # x sort -i idx.h5 --single a.fq b.fq 
    # x sort -i idx.h5 --first a1.fq --second a2.fq --outdir results  # for one FQ file

    # check arguments, either args.single or args.first/args.second
    n = 0
    paired = single = False
    if args.first or args.second:
        paired = True
        if args.guess_pairs and args.first:
            if args.second:
                raise ValueError("ERROR: With given --first, cannot specify --second together with --guess-pairs")
            args.second = guess_pairs(args.first, 2)
        elif args.guess_pairs and args.second:
            args.first = guess_pairs(args.second, 1)
        if len(args.first) != len(args.second):
            raise ValueError("ERROR: --first and --second must specify the same number of files")
            n += len(args.first)
    if args.single:
        single = True
        n += len(args.single)
    print("# sorting {} samples...".format(n))
    if n == 0:
        return  # nothing to do
    if args.outdir and n >= 2:
        raise ValueError("ERROR: cannot specify --outdir with multiple samples")

    print("# loading index {}...".format(args.index))
    idx = load_index(args.index)
    # open statistics file if desired
    if args.statistics:
        print("# collecting statistics at {}...".format(args.statistics))
        with open(args.statistics, "wt") as fstats:
            if single:
                process_single(args, idx, fstats)
            if paired:
                process_paired(args, idx, fstats)
    else:
        if single:
            process_single(args, idx, None)
        if paired:
            process_paired(args, idx, None)


def process_single(args, idx, fstats):
    # process a list of single FASTQ files (unpaired)
    for fqfile in args.single:
        outdir = get_outdir(fqfile, args.outdir)
        fn_graft, fn_host, fn_both, fn_none, fn_amb = get_filenames(outdir)
        with open(fn_graft, "wb") as fgraft, open(fn_host, "wb") as fhost, \
                open(fn_both, "wb") as fboth, open(fn_none, "wb") as fnone, \
                open(fn_amb, "wb") as famb:
            files = (fgraft, fhost, fboth, fnone, famb)  # 0, 1, 2, 3, 4
            for (header, seq, qual) in fastq_reads(fqfile):
                nfile = process_sequence(seq, idx, decider)
                f = files[nfile]
                f.write(h + b'\n' + seq + '\n+\n' + qual + '\n' )

def get_outdir(fname, outdir):
    if outdir is not None:
        return outdir
    # get the base FASTQ name; this is less trivial than it sounds:
    # remove _R1 or _R2, remove .fastq/.fq, .gz ...
    # TODO!
    # collect all basename_R1_00?.fastq.gz
    # single end: just _R1.fastq.gz
    
    return basename
