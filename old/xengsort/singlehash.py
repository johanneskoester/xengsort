"""
Definition of a Single Hash Table
"""

from math import ceil
from collections import namedtuple

import numpy as np
from numba import njit, int64

from .dnaencode import generate_revcomp_and_canonical_code
from .generalhash import generate_get_page_and_get_fingerprint, print_info


SingleHash = namedtuple("SingleHash",
    ["hashtype",
     "hashtable", 
     "get_page",
     "get_fingerprint",
     "store_code",
     "get_genome_for_code",
     "get_occupancy",
     "memorybits",
    ])


def generate_single_hash(q, pagebits, fprbits, pagesize, hf):
    """
    Allocate an array and compile access methods for a hash table.
    Return a SingleHash namedtuple instance.
    
    Parameters:
    q:        q-gram length; all fingerprints are based on q-grams (2q bits)
    pagebits: number of bits for indexing a hash table page;  pagebits < 2*q
    fprbits:  number of bits for fingerprints;  pagebits + fprbits <= 2*q
    pagesize: number of slots in one hash page; the total hash table size is
              2**pagebits * pagesize * ceil((fprbits+2) / 8)
    hf:       name of hash function (string), see generalhash.py
    """

    qbits = 2*q
    if fprbits < 0:
        fprbits = qbits - pagebits    
    if fprbits + pagebits > 2*q or pagebits <= 0:
        raise ValueError("must have 0 < pagebits <= pagebits+fprbits <= 2*q")
    if fprbits > 60:
        raise ValueError("must have fprbits <= 60")
    slotbits = fprbits + 2
    assert slotbits <= 62        
    pagesizebits = slotbits * pagesize  # e.g. 64 for fprbits=14, pagesize=4  

    tablebits = 2**pagebits * pagesizebits
    tablewords = int(ceil(tablebits / 64))
    hashtable = np.zeros(tablewords, dtype=np.int64)
    print_info("SingleHash", slotbits, pagesize, pagesizebits, pagebits, tablewords)
    fprmask = int(2**fprbits - 1)

    # define get_page(code) and get_fingerprint(code) functions
    (get_page, get_fingerprint) = generate_get_page_and_get_fingerprint(
        hf, q, pagebits, fprbits)

    # Extract the item on a given page in a given slot
    @njit(locals=dict(x=int64, mask1=int64, a=int64, b=int64, b1=int64))
    def get_item(table, page, slot):
        # must have 0 <= page < 2**pagebits and 0 <= slot < pagesize
        startbit = page * pagesizebits + slot * slotbits
        a = startbit // 64
        b = startbit & 63
        if b + slotbits <= 64:  # get slotbits bits
            # bits are contained in a single int64
            x = table[a] >> b
        else:
            # bits are distributed over two int64s, 
            # leftmost b1 = 64-b in table[a], rightmost b2 = slotbits-b1 in table[a+1]
            b1 = 64 - b
            mask1 = 2**b1 - 1
            x = (table[a] >> b) & mask1 
            x |= (table[a+1] << b1)
        return x

    @njit(locals=dict(v=int64, v1=int64, startbit=int64, a=int64, b=int64, b1=int64, b2=int64, mask1=int64, mask2=int64))
    def set_item(table, page, slot, f, g):
        # set fingerprint and genome bits to f and g, respectively
        v = (f & fprmask) | ((g&3) << fprbits)  # slotbits
        startbit = page * pagesizebits + slot * slotbits
        a = startbit // 64
        b = startbit & 63
        if b + slotbits <= 64:
            # bits are contained in a single int64
            mask1 = ~((2**slotbits - 1) << b)
            table[a] = (table[a] & mask1) | (v << b)
        else:
            # b1 leftmost bits in table[a] = b1 rightmost bits of v, 
            b1 = 64 - b  # b1 leftmost bits in table[a]
            mask1 = (2**b) - 1  # only keep b rightmost bits
            v1 = (v & (2**b1 - 1))
            table[a] = (table[a] & mask1) | (v1 << b)
            # b2 rightmost bits in table[a+1] = b2 leftmost bits of v
            b2 = slotbits - b1
            mask2 = ~(2**b2 - 1)
            table[a+1] = (table[a+1] & mask2) | (v >> b1)
    
    @njit(locals=dict(code=int64, base=int64, slot=int64, x=int64, f=int64, g=int64))
    def store_code(code, genome, table):
        """
        Attempt to store given code with given genome in hash table.
        Return values:
        -2: Code and genome were already present
        -1: Code was present, new genome information was added
         0: New code was added with given genome information
         1: Overflow, page was full
        """
        page = get_page(code)
        fpr = get_fingerprint(code)
        for slot in range(pagesize):
            x = get_item(table, page, slot)
            g = (x >> fprbits) & 3
            if g == 0:  # empty slot, store here
                set_item(table, page, slot, fpr, genome)
                return 0
            f = x & fprmask
            if f == fpr:
                if g & genome != 0:
                    return -2
                set_item(table, page, slot, fpr, (g | genome))
                return -1
        # all slots were full, but fpr not found -> overflow
        return 1
    
    @njit(locals=dict(code=int64, page=int64, fpr=int64, x=int64, f=int64, g=int64, slot=int64))
    def get_genome_for_code(code, table):
        """
        return genome information (0,1,2,3) for given code,
        or -1 if code was not found but its page is full
        """
        page = get_page(code)
        fpr = get_fingerprint(code)
        for slot in range(pagesize):
            x = get_item(table, page, slot)
            g = (x >> fprbits) & 3
            if g == 0: return 0
            f = x & fprmask
            if f == fpr: return g
        return -1  # not found, but page full
    
    @njit(locals=dict(page=int64, last=int64, slot=int64, x=int64, g=int64))
    def get_occupancy(table):
        pagefill = np.zeros(pagesize+1, dtype=np.int64)
        genomefill = np.zeros(4, dtype=np.int64)
        for page in range(2**pagebits):
            last = -1
            for slot in range(pagesize):
                x = get_item(table, page, slot)
                g = (x >> fprbits) & 3
                genomefill[g] += 1
                if g != 0:
                    last = slot
            pagefill[last+1] += 1
        return pagefill, genomefill

    return SingleHash(
        hashtype="single",
        hashtable=hashtable,
        get_page=get_page,
        get_fingerprint=get_fingerprint,
        store_code=store_code,
        get_genome_for_code=get_genome_for_code,
        get_occupancy=get_occupancy,
        memorybits=tablebits,
        )
